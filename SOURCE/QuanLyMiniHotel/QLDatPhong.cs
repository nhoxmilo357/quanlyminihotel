﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyMiniHotel
{
    public partial class QLDatPhong : Form
    {
        KhachHang kh = new KhachHang();
        DataTable dt;       
        public QLDatPhong()
        {
            InitializeComponent();
        }       
        private void btnThoat_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to exit?", "Xác nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                this.Close();
        }
        void setButton(bool val)
        {
            btncheckin.Enabled = val;
            btncheckout.Enabled = !val;
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lsvDanhsach.SelectedItems.Count > 0)
            {
                lbltenphong.Text = lsvDanhsach.SelectedItems[0].SubItems[0].Text;
                if (lsvDanhsach.SelectedItems[0].ImageIndex == 1)
                {
                    setButton(false);
                }
                else if (lsvDanhsach.SelectedItems[0].ImageIndex == 0)
                {
                    setButton(true);
                }
                if (lsvDanhsach.SelectedItems[0].SubItems[7].Text == radQuaDem.Text)
                {
                    radQuaDem.Text = "QUA ĐÊM";
                    radTheoNgay.Text = "Theo ngày";
                    radTheoGio.Text = "Theo giờ";
                }
                if (lsvDanhsach.SelectedItems[0].SubItems[7].Text == radTheoGio.Text)
                {
                    radTheoGio.Text = "THEO GIỜ";
                    radQuaDem.Text = "Qua đêm";
                    radTheoNgay.Text = "Theo ngày";
                }
                if (lsvDanhsach.SelectedItems[0].SubItems[7].Text == radTheoNgay.Text)
                {
                    radTheoNgay.Text = "THEO NGÀY";
                    radTheoGio.Text = "Theo giờ";
                    radQuaDem.Text = "Qua đêm";
                }
                dtpGioVao.Text = lsvDanhsach.SelectedItems[0].SubItems[6].Text;
                txtHoten.Text = lsvDanhsach.SelectedItems[0].SubItems[1].Text;
                txtCMND.Text = lsvDanhsach.SelectedItems[0].SubItems[2].Text;
                txtDiachi.Text = lsvDanhsach.SelectedItems[0].SubItems[3].Text;
                txtSDT.Text = lsvDanhsach.SelectedItems[0].SubItems[4].Text;
                if (lsvDanhsach.SelectedItems[0].SubItems[5].Text == radNu.Text)
                    radNu.Checked = true;
                if (lsvDanhsach.SelectedItems[0].SubItems[5].Text == radNam.Text)
                    radNam.Checked = true ;
            }
        }
        public void update()
        {
            lsvDanhsach.Items.Clear();
            hienthikhachhang();
            hienthidsmon();
        }
        private void btnThemMon_Click(object sender, EventArgs e)
        {
            if (lsvDanhsach.SelectedItems.Count > 0)
            {
                Order frmorder = new Order(lbltenphong.Text);
                frmorder.cn=update;
                frmorder.Show();
            }
            else MessageBox.Show("Chọn phòng cần thêm món","Thông báo",MessageBoxButtons.OK);
        }

        private void btncheckin_Click(object sender, EventArgs e)
        {
            setButton(false);
            lsvDanhsach.SelectedItems[0].ImageIndex = 1;
        }

        private void btncheckout_Click(object sender, EventArgs e)
        {
            setButton(true);
            lsvDanhsach.SelectedItems[0].ImageIndex = 0;

        }
        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            if (lsvDanhsach.SelectedItems.Count > 0)
            {
                if (lsvDanhsach.SelectedItems[0].ImageIndex == 0)
                {
                    ThanhToan frmtt = new ThanhToan(lbltenphong.Text, txtThoiGian.Text,txtHoten.Text,dtpGioRa.Text);
                    frmtt.cn = update;
                    frmtt.Show();
                }               
                else
                    MessageBox.Show("Bấm Checkout->(Chỉnh sửa ngày giờ vào ra)-> Chọn loại thuê");   
            }
            else { MessageBox.Show("Chọn phòng cần thanh toán", "Thông báo", MessageBoxButtons.OK); }
        }

        private void btnThoat_Click_1(object sender, EventArgs e)
        {
            this.Close();           
        }
        void hienthi()
        {
            dt = kh.laydskhachhang();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListViewItem lsv = lsvDanhsach.Items.Add(dt.Rows[i][0].ToString());
                lsv.SubItems.Add(dt.Rows[i][1].ToString());
                lsv.SubItems.Add(dt.Rows[i][2].ToString());
                lsv.SubItems.Add(dt.Rows[i][3].ToString());
                lsv.SubItems.Add(dt.Rows[i][4].ToString());
                lsv.SubItems.Add(dt.Rows[i][5].ToString());
                lsv.SubItems.Add(dt.Rows[i][6].ToString());
                lsv.SubItems.Add(dt.Rows[i][7].ToString());
                lsv.ImageIndex = 1;
            }
        }
        public void hienthikhachhang()
        {
            hienthi();
        }
        public void hienthidsmon()
        {
            string text1;
            dt = kh.laydsdatmon();
            for (int i = 0; i < lsvDanhsach.Items.Count; i++)
                for (int j = 0; j < dt.Rows.Count; j++)          
                    if (lsvDanhsach.Items[i].Text == dt.Rows[j][0].ToString())
                    {
                        text1 = dt.Rows[j][2].ToString() + " " + dt.Rows[j][1].ToString();
                        for (int x = j+1; x < dt.Rows.Count; x++) 
                        {
                            if (dt.Rows[j][0].ToString()  == dt.Rows[x][0].ToString())
                            {
                                text1 +=","+ dt.Rows[x][2].ToString() + " " + dt.Rows[x][1].ToString();
                            }
                        }
                        lsvDanhsach.Items[i].SubItems.Add(text1);
                    }                  
        }
        void hienthidsphong()
        {
            dt = kh.laydsphong();
            cmbPhong.DataSource = dt;
            cmbPhong.DisplayMember = "tenphong";
        }
        void hienthitreeview()
        {
            dt = kh.layloaiphong();
            DataTable dt1 = kh.laydsphong();
            TreeNode root = new TreeNode("Danh sách phòng");
            root.ExpandAll();
            for(int i=0;i<dt.Rows.Count;i++)
            {
                TreeNode node = new TreeNode(dt.Rows[i][0].ToString());
                node.ExpandAll();
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    if (node.Text == dt1.Rows[j][1].ToString())
                    {
                        TreeNode node1 = new TreeNode(dt1.Rows[j][0].ToString());
                        node.Nodes.Add(node1);
                    } 
                }
                root.Nodes.Add(node); 
            } 
            treeView1.Nodes.Add(root);
        }
        private void QLminihotel_Load(object sender, EventArgs e)
        {
            hienthikhachhang();
            hienthidsphong();
            hienthitreeview();
            hienthidsmon();
            timer1.Start();
            setEnable(false);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dtpGioRa.ShowUpDown = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dtpGioVao.ShowUpDown = true;
        }

        private void radTheoGio_CheckedChanged_1(object sender, EventArgs e)
        {
            DateTime dt1 = DateTime.Parse(dtpGioVao.Text);
            DateTime dt2 = DateTime.Parse(dtpGioRa.Text);
            TimeSpan time = dt2 - dt1;
            if (radTheoGio.Checked || radQuaDem.Checked)
            {                
                if (dtpGioVao.Value.Date.ToString() == dtpGioRa.Value.Date.ToString())
                {
                    txtThoiGian.Text = time.Hours.ToString();
                    lblThoiGian.Text = "Giờ";
                }
                else { txtThoiGian.Text = (Math.Round(Double.Parse(time.TotalHours.ToString()), 0)).ToString(); lblThoiGian.Text = "Giờ"; }
            }
            else if (radTheoNgay.Checked)
            {               
                txtThoiGian.Text = time.Days.ToString();
                lblThoiGian.Text = "Ngày";
            }
        }    
        private void btnFirst_Click_1(object sender, EventArgs e)
        {
            if (lsvDanhsach.SelectedItems.Count > 0)
            {
                if (lsvDanhsach.Items[0].Selected == false)
                {
                    lsvDanhsach.SelectedItems[0].Selected = false;
                    lsvDanhsach.Items[0].Selected = true;
                    lsvDanhsach.Focus();
                }
                else
                {
                    MessageBox.Show("Đã ở vị trí đầu");
                    lsvDanhsach.Focus();
                }
            }
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            if (lsvDanhsach.SelectedItems.Count > 0)
            {
                if (lsvDanhsach.Items[lsvDanhsach.Items.Count - 1].Selected == false)
                {
                    lsvDanhsach.SelectedItems[0].Selected = false;
                    lsvDanhsach.Items[lsvDanhsach.Items.Count - 1].Selected = true;
                    lsvDanhsach.Focus();
                }
                else
                {
                    MessageBox.Show("Đã ở vị trí cuối");
                    lsvDanhsach.Focus();
                }
            }
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            if(lsvDanhsach.SelectedItems.Count>0)
            {
                if(lsvDanhsach.Items[0].Selected==false)
                {
                    int vitri = lsvDanhsach.SelectedItems[0].Index;
                    lsvDanhsach.Items[vitri].Selected = false;
                    lsvDanhsach.Items[vitri - 1].Selected = true;
                    lsvDanhsach.Focus();
                }
                else { MessageBox.Show("Đã ở vị trí đầu"); lsvDanhsach.Focus(); }
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            if(lsvDanhsach.SelectedItems.Count>0)
            {
                if(lsvDanhsach.Items[lsvDanhsach.Items.Count-1].Selected==false)
                {
                    int vitri = lsvDanhsach.SelectedItems[0].Index;
                    lsvDanhsach.Items[vitri].Selected = false;
                    lsvDanhsach.Items[vitri + 1].Selected = true;
                    lsvDanhsach.Focus();
                }
                else { MessageBox.Show("Đã ở vị trí cuối"); lsvDanhsach.Focus(); }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            dt = kh.laydskhachhang();
            DataTable dt1 = kh.laydsphong();
            lsvDanhsach.Items.Clear();
            if(treeView1.SelectedNode.Text=="Danh sách phòng")
            {
                hienthi();
                hienthidsmon();
            }           
            for(int j=0;j<dt1.Rows.Count;j++)
            {
                if(treeView1.SelectedNode.Text==dt1.Rows[j][1].ToString())
                {                  
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i][0].ToString() == dt1.Rows[j][0].ToString())
                        {                            
                            ListViewItem lsv = lsvDanhsach.Items.Add(dt.Rows[i][0].ToString());
                            lsv.SubItems.Add(dt.Rows[i][1].ToString());
                            lsv.SubItems.Add(dt.Rows[i][2].ToString());
                            lsv.SubItems.Add(dt.Rows[i][3].ToString());
                            lsv.SubItems.Add(dt.Rows[i][4].ToString());
                            lsv.SubItems.Add(dt.Rows[i][5].ToString());
                            lsv.SubItems.Add(dt.Rows[i][6].ToString());
                            lsv.SubItems.Add(dt.Rows[i][7].ToString());
                            lsv.ImageIndex = 1;
                            hienthidsmon();
                        }                       
                    }

                }
                else if(treeView1.SelectedNode.Text==dt1.Rows[j][0].ToString())
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i][0].ToString() == dt1.Rows[j][0].ToString())
                        {
                            ListViewItem lsv = lsvDanhsach.Items.Add(dt.Rows[i][0].ToString());
                            lsv.SubItems.Add(dt.Rows[i][1].ToString());
                            lsv.SubItems.Add(dt.Rows[i][2].ToString());
                            lsv.SubItems.Add(dt.Rows[i][3].ToString());
                            lsv.SubItems.Add(dt.Rows[i][4].ToString());
                            lsv.SubItems.Add(dt.Rows[i][5].ToString());
                            lsv.SubItems.Add(dt.Rows[i][6].ToString());
                            lsv.SubItems.Add(dt.Rows[i][7].ToString());
                            lsv.ImageIndex = 1;
                            hienthidsmon();
                        }
                    }
                }
            }
        }
        void setEnable(bool val)
        {
            cmbPhong.Enabled = val;
            txtCMND.Enabled = val;
            txtDiachi.Enabled = val;
            txtHoten.Enabled = val;
            txtSDT.Enabled = val;
            radNam.Enabled = val;
            radNu.Enabled = val;
        }
        void setnull()
        {
            cmbPhong.Text = "";
            txtCMND.Text = "";
            txtDiachi.Text = "";
            txtHoten.Text = "";
            txtSDT.Text = "";
            txtThoiGian.Text = "";
            radNam.Checked = false;
            radNu.Checked = false;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            int flag=0;
            string gender = "", loai="";
            if (radNam.Checked == true)
                gender = "1";
            if (radNu.Checked == true)
                gender = "0";
            if (radQuaDem.Checked == true)
                loai = "Qua đêm";
            if (radTheoGio.Checked == true)
                loai = "Theo giờ";
            if (radTheoNgay.Checked == true)
                loai = "Theo ngày";
            if(btnLuu.Text=="Thêm")
            {
                setEnable(true);
                setnull();
                btnLuu.Text = "Lưu";
            }
            else if (btnLuu.Text == "Lưu")
            {
                if (txtHoten.Text == "" || cmbPhong.Text == "..." || txtCMND.Text == "" || txtDiachi.Text == "" || txtSDT.Text == "" || radNam.Checked == false && radNu.Checked == false)
                    MessageBox.Show("Chưa điền thông tin đầy đủ!(Chọn phòng,nhập tên,CMND,địa chỉ,SĐT,giới tính)");
                else
                {
                    for (int i = 0; i < lsvDanhsach.Items.Count; i++)
                    {
                        if (txtHoten.Text == lsvDanhsach.Items[i].SubItems[1].Text)
                        {
                            MessageBox.Show("Đã có tên khách hàng này! Mời nhập tên khác");
                            flag = 1;
                        }
                    }
                    if (flag == 0)
                    {
                        kh.themkhachhang(txtHoten.Text, txtCMND.Text, txtDiachi.Text, txtSDT.Text, gender, dtpGioVao.Text, loai, cmbPhong.Text);
                        //kh.themthongke1(txtHoten.Text, txtCMND.Text, txtDiachi.Text, txtSDT.Text, gender, dtpGioVao.Text,loai);
                        lsvDanhsach.Items.Clear();
                        hienthikhachhang();
                        hienthidsmon();
                        btnLuu.Text = "Thêm";
                        setEnable(false);
                        setnull();
                    }
                }
            }
        }
        private void cmbPhong_SelectedIndexChanged(object sender, EventArgs e)
        {
            dt = kh.laydskhachhang();
            foreach (DataRow row in dt.Rows)
                if (cmbPhong.Text == row[0].ToString())
                    MessageBox.Show("Phòng này đã được đặt");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            dt = kh.laydskhachhang();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                toolStripStatusLabel1.Text = "Phòng đang sử dụng: " + (i + 1);
                for(int j=0;j<lsvDanhsach.Items.Count;j++)
                    if(lsvDanhsach.Items[j].ImageIndex==0)
                        toolStripStatusLabel1.Text = "Phòng đang sử dụng: " + ((i+1)-1);
            }
            string t = "                                                            Hôm nay  " + DateTime.Now.ToString("dd/MM/yyyy");
            t=t+"  -  "+DateTime.Now.ToString("hh:mm:ss tt");
            toolStripStatusLabel2.Text = t;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            foreach(ListViewItem item in lsvDanhsach.Items)
            {
                if (textBox1.Text == item.SubItems[0].Text || textBox1.Text == item.SubItems[1].Text)
                {
                    lsvDanhsach.Items.Clear();
                    lsvDanhsach.Items.Add(item);
                }
                else if (textBox1.Text == "")
                {
                    lsvDanhsach.Items.Clear();
                    hienthi();
                }
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.ForeColor = Color.Black;
            textBox1.Font = new Font(textBox1.Font.FontFamily, 10);
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            textBox1.Text = "Nhập tên phòng hoặc tên khách";
            textBox1.ForeColor = Color.LightGray;
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            string gt="",lt="";
            if(radNam.Checked==true)
                gt="1";
            if(radNu.Checked==true)
                gt="0";
            if(radTheoGio.Checked==true)
                lt="Theo giờ";
            if(radTheoNgay.Checked==true)
                lt="Theo ngày";
            if(radQuaDem.Checked==true)
                lt="Qua đêm";
            if (lsvDanhsach.SelectedItems.Count > 0)
            {
                if (btnSua.Text == "Sửa")
                {
                    btnSua.Text = "Lưu";
                    setEnable(true);
                }
                else if (btnSua.Text == "Lưu")
                {
                    kh.updateKhachHang(txtHoten.Text, txtCMND.Text, txtDiachi.Text, txtSDT.Text, gt, dtpGioVao.Text,lt,lbltenphong.Text);
                    btnSua.Text = "Sửa";
                    setnull();
                    setEnable(false);
                    lsvDanhsach.Items.Clear();
                    hienthikhachhang();
                    hienthidsmon();
                    MessageBox.Show("Lưu thành công");
                }
            }
            else
                MessageBox.Show("Chọn thông tin cần sửa");
        }

    }
}
