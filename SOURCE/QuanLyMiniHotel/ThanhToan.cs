﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyMiniHotel
{
    public partial class ThanhToan : Form
    {
        KhachHang kh = new KhachHang();
        DataTable dt,dt1,dt2;
        public delegate void capnhat();
        public capnhat cn;
        public ThanhToan()
        {
            InitializeComponent();
        }
        private string message,message1,message2,message3;
        public ThanhToan(string Message,string Message1,string Message2,string Message3): this()
        {
            message = Message;
            message1 = Message1;
            message2 = Message2;
            message3 = Message3;
            lblPhongThanhToan.Text = message;            
        }
       
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ThanhToan_Load(object sender, EventArgs e)
        {
            dt = kh.laydsdatmon();
            dt1 = kh.laydsphong();
            dt2 = kh.laydskhachhang();
            for (int i = 0; i < dt1.Rows.Count; i++)
                if (lblPhongThanhToan.Text == dt1.Rows[i][0].ToString())
                    for (int j = 0; j < dt2.Rows.Count; j++)
                    {
                        if (dt1.Rows[i][1].ToString() == "Phòng lớn")
                        {
                            if (dt1.Rows[i][0].ToString() == dt2.Rows[j][0].ToString() && dt2.Rows[j][7].ToString() == "Theo giờ")
                                txtTienPhong.Text = (Int32.Parse(message1) * 70000).ToString();
                            else if (dt1.Rows[i][0].ToString() == dt2.Rows[j][0].ToString() && dt2.Rows[j][7].ToString() == "Theo ngày")
                                txtTienPhong.Text = (Int32.Parse(message1) * 250000).ToString();
                            else if (dt1.Rows[i][0].ToString() == dt2.Rows[j][0].ToString() && dt2.Rows[j][7].ToString() == "Qua đêm")
                                txtTienPhong.Text = (Int32.Parse(message1) * 120000).ToString();
                        }
                        else if (dt1.Rows[i][1].ToString() == "Phòng nhỏ")
                        {
                            if (dt1.Rows[i][0].ToString() == dt2.Rows[j][0].ToString() && dt2.Rows[j][7].ToString() == "Theo giờ")
                                txtTienPhong.Text = (Int32.Parse(message1) * 50000).ToString();
                            else if (dt1.Rows[i][0].ToString() == dt2.Rows[j][0].ToString() && dt2.Rows[j][7].ToString() == "Theo ngày")
                                txtTienPhong.Text = (Int32.Parse(message1) * 200000).ToString();
                            else if (dt1.Rows[i][0].ToString() == dt2.Rows[j][0].ToString() && dt2.Rows[j][7].ToString() == "Qua đêm")
                                txtTienPhong.Text = (Int32.Parse(message1) * 100000).ToString();
                        }
                    }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (lblPhongThanhToan.Text == dt.Rows[i]["tenphong"].ToString())
                {
                    if (dt.Rows[i][5].ToString() == "Thức Uống")
                        txtThucUong.Text = dt.Rows[i][4].ToString();
                    if (dt.Rows[i][5].ToString() == "Thức Ăn")
                        txtThucAn.Text = dt.Rows[i][4].ToString();
                }
                txtTongTien.Text = (Int32.Parse(txtTienPhong.Text) + Int32.Parse(txtThucAn.Text) + Int32.Parse(txtThucUong.Text)).ToString();           
            }
        }
        private void txtKhachDua_TextChanged(object sender, EventArgs e)
        {
            txtTienThoi.Text = (Int32.Parse(txtKhachDua.Text) - Int32.Parse(txtTongTien.Text)).ToString();
        }

        private void btnThanhToan_Click(object sender, EventArgs e)
        {
            string tiendatmon = (Int32.Parse(txtThucAn.Text) + Int32.Parse(txtThucUong.Text)).ToString();
            kh.themthongkett(lblPhongThanhToan.Text, message3, message1, txtTienPhong.Text, tiendatmon, txtTongTien.Text, txtKhachDua.Text, txtTienThoi.Text);
            if (Int32.Parse(txtTienThoi.Text) < 0)
                MessageBox.Show("Tiền khách đưa chưa đủ");
            else
            {
                DialogResult dr = MessageBox.Show("     HÓA ĐƠN THANH TOÁN\n\n" + lblPhongThanhToan.Text + "\nHọ tên:  " + message2 + "\n--------------------------------"
                    + "\nTiền phòng:\t" + txtTienPhong.Text + "\nTiền thức ăn:\t" + txtThucAn.Text + "\nTiền thức uống:\t" + txtThucUong.Text
                    + "\n--------------------------------" + "\nTổng cộng:\t" + txtTongTien.Text + "\nTiền mặt:\t" + "\t" + txtKhachDua.Text
                    + "\nTiền thừa:\t" + "\t" + txtTienThoi.Text + "\n--------------------------------" + "\nHOÀN TẤT THANH TOÁN?", "HÓA ĐƠN THANH TOÁN", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    kh.xoakhachhang(message2);
                    kh.xoamon(lblPhongThanhToan.Text);
                    cn.Invoke();
                    this.Close();
                }
            }           
        }
    }
}
