﻿namespace QuanLyMiniHotel
{
    partial class ThongKe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.clmPhong = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmHoTen = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmCMND = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmDiaChi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmSDT = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmGioiTinh = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmGioVao = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmGioRa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmSoGioNgay = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmLoaiThue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmTienPhong = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmTienThucAn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmTongTien = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmKhachDua = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.clmTienThoi = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(165, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tìm kiếm";
            // 
            // textBox1
            // 
            this.textBox1.ForeColor = System.Drawing.Color.LightGray;
            this.textBox1.Location = new System.Drawing.Point(255, 13);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(845, 29);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "Nhập tên phòng hoặc tên khách hàng";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.clmPhong,
            this.clmHoTen,
            this.clmCMND,
            this.clmDiaChi,
            this.clmSDT,
            this.clmGioiTinh,
            this.clmGioVao,
            this.clmGioRa,
            this.clmSoGioNgay,
            this.clmLoaiThue,
            this.clmTienPhong,
            this.clmTienThucAn,
            this.clmTongTien,
            this.clmKhachDua,
            this.clmTienThoi});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(4, 162);
            this.listView1.Margin = new System.Windows.Forms.Padding(4);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1232, 468);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // clmPhong
            // 
            this.clmPhong.Text = "Phòng";
            this.clmPhong.Width = 80;
            // 
            // clmHoTen
            // 
            this.clmHoTen.Text = "Họ tên";
            this.clmHoTen.Width = 200;
            // 
            // clmCMND
            // 
            this.clmCMND.Text = "CMND";
            this.clmCMND.Width = 80;
            // 
            // clmDiaChi
            // 
            this.clmDiaChi.Text = "Địa chỉ";
            this.clmDiaChi.Width = 250;
            // 
            // clmSDT
            // 
            this.clmSDT.Text = "Số điện thoại";
            this.clmSDT.Width = 100;
            // 
            // clmGioiTinh
            // 
            this.clmGioiTinh.Text = "Giới tính";
            this.clmGioiTinh.Width = 70;
            // 
            // clmGioVao
            // 
            this.clmGioVao.Text = "Giờ vào";
            this.clmGioVao.Width = 150;
            // 
            // clmGioRa
            // 
            this.clmGioRa.Text = "Giờ ra";
            this.clmGioRa.Width = 150;
            // 
            // clmSoGioNgay
            // 
            this.clmSoGioNgay.Text = "Số Giờ/Ngày";
            this.clmSoGioNgay.Width = 100;
            // 
            // clmLoaiThue
            // 
            this.clmLoaiThue.Text = "Loại thuê";
            this.clmLoaiThue.Width = 80;
            // 
            // clmTienPhong
            // 
            this.clmTienPhong.Text = "Tiền phòng";
            this.clmTienPhong.Width = 100;
            // 
            // clmTienThucAn
            // 
            this.clmTienThucAn.Text = "Tiền thức ăn";
            this.clmTienThucAn.Width = 100;
            // 
            // clmTongTien
            // 
            this.clmTongTien.Text = "Tổng chi phí";
            this.clmTongTien.Width = 150;
            // 
            // clmKhachDua
            // 
            this.clmKhachDua.Text = "Khách trả";
            this.clmKhachDua.Width = 100;
            // 
            // clmTienThoi
            // 
            this.clmTienThoi.Text = "Tiền thừa";
            this.clmTienThoi.Width = 80;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(264, 111);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(749, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "Bảng thống kê lịch sử danh sách khách hàng tại KS Mini LÂU DÀI";
            // 
            // ThongKe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(1241, 635);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ThongKe";
            this.Text = "Thống kê lịch sử khách hàng";
            this.Load += new System.EventHandler(this.ThongKe_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader clmPhong;
        private System.Windows.Forms.ColumnHeader clmHoTen;
        private System.Windows.Forms.ColumnHeader clmCMND;
        private System.Windows.Forms.ColumnHeader clmDiaChi;
        private System.Windows.Forms.ColumnHeader clmSDT;
        private System.Windows.Forms.ColumnHeader clmGioiTinh;
        private System.Windows.Forms.ColumnHeader clmGioVao;
        private System.Windows.Forms.ColumnHeader clmGioRa;
        private System.Windows.Forms.ColumnHeader clmSoGioNgay;
        private System.Windows.Forms.ColumnHeader clmLoaiThue;
        private System.Windows.Forms.ColumnHeader clmTienPhong;
        private System.Windows.Forms.ColumnHeader clmTienThucAn;
        private System.Windows.Forms.ColumnHeader clmTongTien;
        private System.Windows.Forms.ColumnHeader clmKhachDua;
        private System.Windows.Forms.ColumnHeader clmTienThoi;
        private System.Windows.Forms.Label label2;
    }
}