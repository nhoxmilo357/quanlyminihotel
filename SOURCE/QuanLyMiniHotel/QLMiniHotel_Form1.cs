﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyMiniHotel
{
    public partial class QLMiniHotel_Form1 : Form
    {
        KhachHang kh=new KhachHang();
        public QLMiniHotel_Form1()
        {
            InitializeComponent();
        }    
        DangNhap frmdn = new DangNhap();
        void set()
        {
            frmdn.Close();
            toolStrip1.Enabled = true;
        }
        private void QLMiniHotel_Form1_Load(object sender, EventArgs e)
        {
            frmdn.ql = set;
            frmdn.ShowDialog();
        }
        private void tStriplblNhanVien_Click(object sender, EventArgs e)
        {
            QLNV frmnv = new QLNV();
            frmnv.MdiParent = this;
            frmnv.Show();
        }

        private void tStriplblDatPhong_Click(object sender, EventArgs e)
        {
            QLDatPhong frmdp = new QLDatPhong();
            frmdp.MdiParent = this;
            frmdp.Show();
        }

        private void tStriplblBangGia_Click(object sender, EventArgs e)
        {
            Banggia frmbg = new Banggia();
            frmbg.MdiParent = this;
            frmbg.Show();
        }

        private void tStriplblThongTin_Click(object sender, EventArgs e)
        {
            About frmtt = new About();
            frmtt.MdiParent = this;
            frmtt.Show();
        }

        private void tStriplblThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ThongKe frmtk = new ThongKe();
            frmtk.MdiParent = this;
            frmtk.Show();
        }
        
    }
}
