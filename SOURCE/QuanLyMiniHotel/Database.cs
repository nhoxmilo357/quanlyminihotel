﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace QuanLyMiniHotel
{
    class Database
    {
        SqlConnection con;
        SqlDataAdapter da;
        DataSet ds;
        public Database()
        {
            string strcon = @"Data Source=DESKTOP-6B6RDD2\SQLEXPRESS;Initial Catalog=QLHotelMini;Integrated Security=True";
            con = new SqlConnection(strcon);
        }
        public DataTable truyvan(string sql)
        {
            da = new SqlDataAdapter(sql, con);
            ds = new DataSet();
            da.Fill(ds);
            return ds.Tables[0];
        }
        public void Lenh(string sql)
        {
            SqlCommand cmd = new SqlCommand(sql, con);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
