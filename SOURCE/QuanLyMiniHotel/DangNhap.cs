﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyMiniHotel
{
    public partial class DangNhap : Form
    {
        DataTable dt;
        KhachHang kh=new KhachHang();
        public delegate void QL();
        public QL ql;
        public DangNhap()
        {
            InitializeComponent();
        }
        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            dt = kh.laytaikhoan();
            int flag = 0;
            foreach (DataRow row in dt.Rows)
            {

                if (txtTK.Text == row[0].ToString() && txtMK.Text == row[1].ToString())
                {
                    ql.Invoke();
                    flag = 1;
                }
            }
            if (flag == 0)
                MessageBox.Show("Đăng nhập thất bại");
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
