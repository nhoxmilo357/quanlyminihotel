﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyMiniHotel
{
    public partial class QLNV : Form
    {
        KhachHang kh = new KhachHang();
        DataTable dt;
        public QLNV()
        {
            InitializeComponent();           
        }        
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void hienthinhanvien()
        {
            dt = kh.laydsnhanvien();
            for(int i=0;i<dt.Rows.Count;i++)
            {
                ListViewItem lvi = lsvQLNV.Items.Add(dt.Rows[i][0].ToString());
                lvi.SubItems.Add(dt.Rows[i][1].ToString());
                lvi.SubItems.Add(dt.Rows[i][2].ToString());
                lvi.SubItems.Add(dt.Rows[i][3].ToString());
                lvi.SubItems.Add(dt.Rows[i][4].ToString());
                lvi.SubItems.Add(dt.Rows[i][5].ToString());
                lvi.SubItems.Add(dt.Rows[i][6].ToString());
            }
        }
        void hienthivaitro()
        {
            dt = kh.layvaitro();
            cmbVaitro.DataSource = dt;
            cmbVaitro.DisplayMember = "vaitro";
            cmbVaitro.ValueMember = "mavaitro";
        }
        private void QLNV_Load(object sender, EventArgs e)
        {
            hienthinhanvien();
            hienthivaitro();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            int flag = 0;
            if(btnThem.Text=="Thêm")
            {
                btnThem.Text = "Lưu";
                txtDiaChi.Text = "";
                txtHoTen.Text = "";
                txtSDT.Text = "";
                radNam.Checked = false;
                radNu.Checked = false;
                cmbVaitro.Text = "";
            }
            else if (btnThem.Text == "Lưu")
            {
                if (txtHoTen.Text == "" || txtDiaChi.Text == "" || txtSDT.Text == "" || radNam.Checked == false && radNu.Checked == false)
                {
                    MessageBox.Show("Chưa điền thông tin đầy đủ");
                }
                else
                {
                    for (int i = 0; i < lsvQLNV.Items.Count; i++)
                    {
                        if (cmbVaitro.Text == lsvQLNV.Items[i].SubItems[5].Text && cmbVaitro.Text != "Phục vụ phòng")
                        {
                            MessageBox.Show("Đã có vai trò này !");
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 0)
                    {
                        string gender = "";
                        if (radNam.Checked == true)
                            gender = "1";
                        if (radNu.Checked == true)
                            gender = "0";
                        kh.themnhanvien(txtHoTen.Text, dtpNgaySinh.Text, txtDiaChi.Text, txtSDT.Text, gender, cmbVaitro.SelectedValue.ToString());
                        lsvQLNV.Items.Clear();
                        hienthinhanvien();
                        btnThem.Text = "Thêm";
                    }
                }
            }
        }

        private void lsvQLNV_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lsvQLNV.SelectedItems.Count>0)
            {
                txtHoTen.Text = lsvQLNV.SelectedItems[0].SubItems[0].Text;
                dtpNgaySinh.Text = lsvQLNV.SelectedItems[0].SubItems[1].Text;
                txtDiaChi.Text = lsvQLNV.SelectedItems[0].SubItems[2].Text;
                txtSDT.Text = lsvQLNV.SelectedItems[0].SubItems[3].Text;
                if (lsvQLNV.SelectedItems[0].SubItems[4].Text == "Nam")
                    radNam.Checked = true;
                if (lsvQLNV.SelectedItems[0].SubItems[4].Text == "Nữ")
                    radNu.Checked = true;
                cmbVaitro.Text = lsvQLNV.SelectedItems[0].SubItems[5].Text;
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lsvQLNV.SelectedItems.Count > 0)
            {
                DialogResult dr = MessageBox.Show("Bạn có chắc muốn xóa nhân viên này?", "Xóa nhân viên", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    kh.xoanhanvien(lsvQLNV.SelectedItems[0].SubItems[0].Text);
                    lsvQLNV.Items.RemoveAt(lsvQLNV.SelectedIndices[0]);
                    txtHoTen.Text = "";
                    txtDiaChi.Text = "";
                    txtSDT.Text = "";
                    cmbVaitro.Text = "";
                    dtpNgaySinh.Text = "";
                    radNam.Checked = false;
                    radNu.Checked = false;
                }
            }
            else
                MessageBox.Show("Chọn nhân viên muốn xóa","Xóa nhân viên",MessageBoxButtons.OK);
        }     
    }
}
