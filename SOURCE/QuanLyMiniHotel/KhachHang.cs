﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QuanLyMiniHotel
{
    class KhachHang
    {
        Database db;
        public KhachHang()
        {
            db = new Database();
        }
        public DataTable laydskhachhang()
        {            
            string sql = "select tenphong,tenkh,cmnd,diachi,sodt,gioitinh=case gioitinh when 'true' then N'Nam' when 'false' then N'Nữ' end,giovao,loaithue from KhachHang k,phong p where k.maphong=p.maphong";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        public DataTable laydsorder()
        {
            string sql = "select * from oder";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        public DataTable laydsphong()
        {
            string sql = "select * from phong";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        public DataTable layloaiphong()
        {
            string sql = "select * from loaiphong";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        public DataTable laygiaphong()
        {
            string sql = "select * from gia";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        public DataTable laydsnhanvien()
        {
           string sql = "select ten,ngaysinh,diachi,sodt,gioitinh=case gioitinh when 'true' then N'Nam' when 'false' then N'Nữ' end,vaitro,luong from nhanvien n,luongnv l where n.mavaitro=l.mavaitro";
           DataTable dt=db.truyvan(sql);
           return dt;
        }      
        public DataTable laydsdatmon()
        {
            string sql = "select * from oderfood";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        public DataTable layvaitro()
        {
            string sql = "select * from luongnv";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        public DataTable laytaikhoan()
        {
            string sql = "select * from nguoidung";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        //public DataTable laydsthongkekh()
        //{
        //    string sql = "select maphong,tenkh,cmnd,diachi,sodt,gioitinh=case gioitinh when 'true' then N'Nam' when 'false' then N'Nữ' end,giovao,loaithue from KhachHang";
        //    DataTable dt = db.truyvan(sql);
        //    return dt;
        //}
        public DataTable laydsthongkett()
        {
            string sql = "select * from thongkethanhtoan";
            DataTable dt = db.truyvan(sql);
            return dt;
        }
        //public DataTable laydsthongkedm()
        //{
        //    string sql = "select * from thongkedatmon";
        //    DataTable dt = db.truyvan(sql);
        //    return dt;
        //}
        public void themkhachhang(string ten,string cmnd,string diachi,string sdt,string gioitinh,string giovao,string loai,string tenphong)
        {
            string sql = string.Format("INSERT into khachhang values(N'{0}','{1}',N'{2}','{3}',N'{4}','{5}',N'{6}',N'{7}')", ten, cmnd, diachi, sdt, gioitinh, giovao, loai, tenphong);
            //string SQL = string.Format("INSERT into thongkekh values(N'{0}',N'{1}','{2}',N'{3}','{4}','{5}','{6}',N'{7}')",tenphong, ten, cmnd, diachi, sdt, gioitinh, giovao,loai);
            db.Lenh(sql);
            //db.Lenh(SQL);
        }
        public void themthongkett(string tenphong, string giora, string so, string tienphong, string tiendatmon, string tongtien,string khachdua,string tienthoi)
        {
            string SQL = string.Format("INSERT into thongkethanhtoan values(N'{1}','{2}','{3}',{4},{5},{6},{7},{8})", tenphong, giora, so, tienphong,tiendatmon,tongtien,khachdua,tienthoi);
            db.Lenh(SQL);
        }
        //public void themthongkedatmon(string tenphong,string mon)
        //{
        //    string SQL = string.Format("insert into thongkedatmon values(N'{0}',N'{1}')",tenphong,mon);
        //    db.Lenh(SQL);
        //}
        public void themmon(string mathang,string sl,string gia,string tt,string tenphong,string loai)
        {
            string sql = string.Format("insert into oderfood values(N'{0}',N'{1}',{2},{3},{4},N'{5}')",tenphong, mathang, sl, gia, tt,loai); 
            db.Lenh(sql);         
        }
        public void themnhanvien(string ten, string ngaysinh,string diachi, string sdt,string gioitinh, string index_vaitro)
        {
            string sql = string.Format("insert into nhanvien values(N'{0}','{1}',N'{2}',{3},'{4}',{5})", ten, ngaysinh, diachi, sdt, gioitinh, index_vaitro);
            db.Lenh(sql);
        }
        public void xoanhanvien(string ten)
        {
            string sql = "delete from nhanvien where ten=N'" +ten+"'";
            db.Lenh(sql);
        }
        public void xoakhachhang(string ten)
        {
            string sql = "delete from khachhang where tenkh=N'" + ten + "'";
            db.Lenh(sql);
        }
        public void xoamon(string phong)
        {
            string sql = "delete from oderfood where tenphong=N'" + phong + "'";
            //string SQL = "delete from thongkedatmon where tenphong=N'" + phong + "'";
            db.Lenh(sql);
            //db.Lenh(SQL);
        }
        public void updateKhachHang(string ten, string cmnd,string diachi, string sdt,string gioitinh,string giovao,string loaithue, string tenphong)
        {
            string sql = string.Format("update khachhang set tenkh=N'{0}',cmnd='{1}',diachi=N'{2}',sodt='{3}',gioitinh='{4}',giovao='{5}',loaithue=N'{6}' where tenphong=N'{7}'", ten,cmnd, diachi, sdt, gioitinh,giovao,loaithue, tenphong);
            //string SQL = string.Format("update thongkekh set tenkh=N'{1}',cmnd='{2}',diachi=N'{3}',sodt='{4}',gioitinh='{5}',giovao='{6}',loaithue=N'{7}' where tenphong=N'{0}'", tenphong,ten, cmnd, diachi, sdt, gioitinh, giovao, loaithue);
            db.Lenh(sql);
            //db.Lenh(SQL);
        }
    }
}
