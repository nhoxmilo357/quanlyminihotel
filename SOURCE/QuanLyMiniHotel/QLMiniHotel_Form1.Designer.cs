﻿namespace QuanLyMiniHotel
{
    partial class QLMiniHotel_Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QLMiniHotel_Form1));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel6 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel7 = new System.Windows.Forms.ToolStripLabel();
            this.tStriplblNhanVien = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel9 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel10 = new System.Windows.Forms.ToolStripLabel();
            this.tStriplblDatPhong = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel12 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel13 = new System.Windows.Forms.ToolStripLabel();
            this.tStriplblBangGia = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel15 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel16 = new System.Windows.Forms.ToolStripLabel();
            this.tStriplblThongTin = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel18 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel8 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel11 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel19 = new System.Windows.Forms.ToolStripLabel();
            this.tStriplblThoat = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Enabled = false;
            this.toolStrip1.Font = new System.Drawing.Font("Rockwell", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.ImeMode = System.Windows.Forms.ImeMode.Hiragana;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripLabel2,
            this.toolStripLabel3,
            this.toolStripLabel4,
            this.toolStripLabel5,
            this.toolStripLabel6,
            this.toolStripLabel7,
            this.tStriplblNhanVien,
            this.toolStripLabel9,
            this.toolStripLabel10,
            this.tStriplblDatPhong,
            this.toolStripLabel12,
            this.toolStripLabel13,
            this.tStriplblBangGia,
            this.toolStripLabel15,
            this.toolStripLabel16,
            this.tStriplblThongTin,
            this.toolStripLabel18,
            this.toolStripLabel8,
            this.toolStripButton1,
            this.toolStripLabel11,
            this.toolStripLabel19,
            this.tStriplblThoat});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(1051, 74);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLabel1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel1.Image")));
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(20, 20);
            this.toolStripLabel1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(0, 0);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.None;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(0, 0);
            this.toolStripLabel3.Text = "toolStripLabel3";
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(0, 0);
            this.toolStripLabel4.Text = "toolStripLabel4";
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(399, 37);
            this.toolStripLabel5.Text = "KHÁCH SẠN MINI LÂU DÀI";
            // 
            // toolStripLabel6
            // 
            this.toolStripLabel6.Name = "toolStripLabel6";
            this.toolStripLabel6.Size = new System.Drawing.Size(252, 17);
            this.toolStripLabel6.Text = "                                                             ";
            // 
            // toolStripLabel7
            // 
            this.toolStripLabel7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLabel7.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripLabel7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel7.Image")));
            this.toolStripLabel7.Name = "toolStripLabel7";
            this.toolStripLabel7.Size = new System.Drawing.Size(20, 20);
            this.toolStripLabel7.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            this.toolStripLabel7.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            // 
            // tStriplblNhanVien
            // 
            this.tStriplblNhanVien.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tStriplblNhanVien.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tStriplblNhanVien.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tStriplblNhanVien.Name = "tStriplblNhanVien";
            this.tStriplblNhanVien.Size = new System.Drawing.Size(110, 31);
            this.tStriplblNhanVien.Text = "Nhân viên";
            this.tStriplblNhanVien.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.tStriplblNhanVien.Click += new System.EventHandler(this.tStriplblNhanVien_Click);
            // 
            // toolStripLabel9
            // 
            this.toolStripLabel9.Name = "toolStripLabel9";
            this.toolStripLabel9.Size = new System.Drawing.Size(36, 17);
            this.toolStripLabel9.Tag = "  ";
            this.toolStripLabel9.Text = "       ";
            // 
            // toolStripLabel10
            // 
            this.toolStripLabel10.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel10.Image")));
            this.toolStripLabel10.Name = "toolStripLabel10";
            this.toolStripLabel10.Size = new System.Drawing.Size(20, 20);
            // 
            // tStriplblDatPhong
            // 
            this.tStriplblDatPhong.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tStriplblDatPhong.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tStriplblDatPhong.Image = ((System.Drawing.Image)(resources.GetObject("tStriplblDatPhong.Image")));
            this.tStriplblDatPhong.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tStriplblDatPhong.Name = "tStriplblDatPhong";
            this.tStriplblDatPhong.Size = new System.Drawing.Size(112, 31);
            this.tStriplblDatPhong.Text = "Đặt phòng";
            this.tStriplblDatPhong.Click += new System.EventHandler(this.tStriplblDatPhong_Click);
            // 
            // toolStripLabel12
            // 
            this.toolStripLabel12.Name = "toolStripLabel12";
            this.toolStripLabel12.Size = new System.Drawing.Size(36, 17);
            this.toolStripLabel12.Text = "       ";
            // 
            // toolStripLabel13
            // 
            this.toolStripLabel13.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel13.Image")));
            this.toolStripLabel13.Name = "toolStripLabel13";
            this.toolStripLabel13.Size = new System.Drawing.Size(20, 20);
            // 
            // tStriplblBangGia
            // 
            this.tStriplblBangGia.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tStriplblBangGia.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tStriplblBangGia.Image = ((System.Drawing.Image)(resources.GetObject("tStriplblBangGia.Image")));
            this.tStriplblBangGia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tStriplblBangGia.Name = "tStriplblBangGia";
            this.tStriplblBangGia.Size = new System.Drawing.Size(156, 31);
            this.tStriplblBangGia.Text = "Bảng giá phòng";
            this.tStriplblBangGia.Click += new System.EventHandler(this.tStriplblBangGia_Click);
            // 
            // toolStripLabel15
            // 
            this.toolStripLabel15.Name = "toolStripLabel15";
            this.toolStripLabel15.Size = new System.Drawing.Size(36, 17);
            this.toolStripLabel15.Text = "       ";
            // 
            // toolStripLabel16
            // 
            this.toolStripLabel16.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel16.Image")));
            this.toolStripLabel16.Name = "toolStripLabel16";
            this.toolStripLabel16.Size = new System.Drawing.Size(20, 20);
            // 
            // tStriplblThongTin
            // 
            this.tStriplblThongTin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tStriplblThongTin.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tStriplblThongTin.Image = ((System.Drawing.Image)(resources.GetObject("tStriplblThongTin.Image")));
            this.tStriplblThongTin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tStriplblThongTin.Name = "tStriplblThongTin";
            this.tStriplblThongTin.Size = new System.Drawing.Size(228, 31);
            this.tStriplblThongTin.Text = "Thông tin chương trình";
            this.tStriplblThongTin.Click += new System.EventHandler(this.tStriplblThongTin_Click);
            // 
            // toolStripLabel18
            // 
            this.toolStripLabel18.Name = "toolStripLabel18";
            this.toolStripLabel18.Size = new System.Drawing.Size(36, 17);
            this.toolStripLabel18.Text = "       ";
            // 
            // toolStripLabel8
            // 
            this.toolStripLabel8.BackColor = System.Drawing.Color.Transparent;
            this.toolStripLabel8.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel8.Image")));
            this.toolStripLabel8.Name = "toolStripLabel8";
            this.toolStripLabel8.Size = new System.Drawing.Size(20, 20);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(100, 31);
            this.toolStripButton1.Text = "Thống kê";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.toolStripButton1.ToolTipText = "\r\n";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripLabel11
            // 
            this.toolStripLabel11.Name = "toolStripLabel11";
            this.toolStripLabel11.Size = new System.Drawing.Size(36, 17);
            this.toolStripLabel11.Text = "       ";
            // 
            // toolStripLabel19
            // 
            this.toolStripLabel19.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel19.Image")));
            this.toolStripLabel19.Name = "toolStripLabel19";
            this.toolStripLabel19.Size = new System.Drawing.Size(20, 20);
            // 
            // tStriplblThoat
            // 
            this.tStriplblThoat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tStriplblThoat.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tStriplblThoat.Image = ((System.Drawing.Image)(resources.GetObject("tStriplblThoat.Image")));
            this.tStriplblThoat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tStriplblThoat.Name = "tStriplblThoat";
            this.tStriplblThoat.Size = new System.Drawing.Size(68, 31);
            this.tStriplblThoat.Text = "Thoát";
            this.tStriplblThoat.Click += new System.EventHandler(this.tStriplblThoat_Click);
            // 
            // QLMiniHotel_Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1051, 508);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "QLMiniHotel_Form1";
            this.Text = "Quản lý khách sạn mini";
            this.TransparencyKey = System.Drawing.SystemColors.ControlText;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.QLMiniHotel_Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripLabel toolStripLabel6;
        private System.Windows.Forms.ToolStripLabel toolStripLabel7;
        private System.Windows.Forms.ToolStripLabel toolStripLabel9;
        private System.Windows.Forms.ToolStripLabel toolStripLabel10;
        private System.Windows.Forms.ToolStripLabel toolStripLabel12;
        private System.Windows.Forms.ToolStripLabel toolStripLabel13;
        private System.Windows.Forms.ToolStripLabel toolStripLabel15;
        private System.Windows.Forms.ToolStripLabel toolStripLabel16;
        private System.Windows.Forms.ToolStripLabel toolStripLabel18;
        private System.Windows.Forms.ToolStripLabel toolStripLabel19;
        private System.Windows.Forms.ToolStripButton tStriplblNhanVien;
        private System.Windows.Forms.ToolStripButton tStriplblDatPhong;
        private System.Windows.Forms.ToolStripButton tStriplblBangGia;
        private System.Windows.Forms.ToolStripButton tStriplblThongTin;
        private System.Windows.Forms.ToolStripButton tStriplblThoat;
        private System.Windows.Forms.ToolStripLabel toolStripLabel8;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel11;
    }
}

