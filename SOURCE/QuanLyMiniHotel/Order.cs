﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyMiniHotel
{
    public partial class Order : Form
    {
        KhachHang kh=new KhachHang();
        public Order()
        {
            InitializeComponent();
        }
        private string message;
        public delegate void capnhatdulieu();
        public capnhatdulieu cn;
        public Order(string Message): this()
        {
            message = Message;
            lblPhong.Text = message;
        }
        void hienthiorder()
        {
            DataTable dt = kh.laydsorder();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListViewItem lsv = lsv1.Items.Add(dt.Rows[i][0].ToString());
                lsv.SubItems.Add(dt.Rows[i][1].ToString());
                lsv.SubItems.Add(dt.Rows[i][2].ToString());
                lsv.SubItems.Add(dt.Rows[i][3].ToString());
                lsv.SubItems.Add(dt.Rows[i][4].ToString());
            }
        }
        void hienthimon()
        {
            DataTable dt = kh.laydsdatmon();
            foreach (DataRow row in dt.Rows)
                if (lblPhong.Text == row[0].ToString())
                {
                    ListViewItem item = new ListViewItem();
                    item.SubItems.Add(row[5].ToString());
                    item.SubItems.Add(row[1].ToString());
                    item.SubItems.Add(row[2].ToString());
                    item.SubItems.Add(row[3].ToString());
                    item.SubItems.Add(row[4].ToString());
                    lsv2.Items.Add(item);
                    for (int i = 0; i < lsv2.Items.Count; i++)
                        if (lsv2.Items[i].SubItems[1].Text != null)
                            lsv2.Items[i].Text = (i + 1).ToString();
                }
        }
        private void Order_Load(object sender, EventArgs e)
        {
            hienthiorder();
            hienthimon();
            timer1.Start();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
                if (lsv1.SelectedItems.Count > 0)
                {
                    txtMatHang.Text = lsv1.SelectedItems[0].SubItems[2].Text;
                    txtDonGia.Text = lsv1.SelectedItems[0].SubItems[3].Text;
                }
        }
        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnThem_Click(object sender, EventArgs e)
        {
            int dem=(int)numericUpDown1.Value;
            int tong=dem*Int32.Parse( txtDonGia.Text);
            if (lsv1.SelectedItems.Count > 0)
            {
                if (numericUpDown1.Value == 0)
                {
                    MessageBox.Show("Nhập số lượng");
                }
                else
                {
                    ListViewItem lvi = new ListViewItem();
                    lvi.SubItems.Add(lsv1.SelectedItems[0].SubItems[1].Text);
                    lvi.SubItems.Add(txtMatHang.Text);
                    lvi.SubItems.Add(dem.ToString());
                    lvi.SubItems.Add(txtDonGia.Text);
                    lvi.SubItems.Add(tong.ToString());
                    lsv2.Items.Add(lvi);
                }
            }
            for (int i = 0; i < lsv2.Items.Count; i++)
                if (lsv2.Items[i].SubItems[1].Text != null)
                    lsv2.Items[i].Text=(i+1).ToString();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            kh.xoamon(lblPhong.Text);   
            for(int i=0;i<lsv2.Items.Count;i++)
                kh.themmon(lsv2.Items[i].SubItems[2].Text, lsv2.Items[i].SubItems[3].Text, lsv2.Items[i].SubItems[4].Text, lsv2.Items[i].SubItems[5].Text,lblPhong.Text, lsv2.Items[i].SubItems[1].Text);
            MessageBox.Show("Đã lưu");
            cn.Invoke();
            //kh.themthongkedatmon(lblPhong.Text,message1);
            this.Close();
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (lsv2.SelectedItems.Count > 0)
                lsv2.Items.RemoveAt(lsv2.SelectedIndices[0]);
            else
                MessageBox.Show("Chọn món cần xóa");
            for (int i = 0; i < lsv2.Items.Count; i++)
                if (lsv2.Items[i].SubItems[1].Text != null)
                    lsv2.Items[i].Text = (i+1).ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            for (int j = 0; j < lsv2.Items.Count; j++)
                toolStripStatusLabel2.Text = "Đã đặt " + (j + 1) + " món";
            for (int i = 0; i < lsv1.Items.Count; i++)
                toolStripStatusLabel1.Text = (i + 1) + " Mặt hàng     ";
        }
    }
}
