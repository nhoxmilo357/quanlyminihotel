﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuanLyMiniHotel
{
    public partial class Banggia : Form
    {
        KhachHang kh = new KhachHang();
        public Banggia()
        {
            InitializeComponent();
        }
        void hienbanggia()
        {
            DataTable dt = kh.laygiaphong();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ListViewItem lvi = lsvGia.Items.Add(dt.Rows[i][0].ToString());
                lvi.SubItems.Add(dt.Rows[i][1].ToString());
                lvi.SubItems.Add(dt.Rows[i][2].ToString());
                lvi.SubItems.Add(dt.Rows[i][3].ToString());
            }
        }
        private void Banggia_Load(object sender, EventArgs e)
        {
            hienbanggia();
        }
    }
}
