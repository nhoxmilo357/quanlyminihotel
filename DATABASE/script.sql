USE [master]
GO
/****** Object:  Database [QLHotelMini]    Script Date: 11/3/2017 6:31 PM ******/
CREATE DATABASE [QLHotelMini]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLHotelMini', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLHotelMini.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLHotelMini_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLHotelMini_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLHotelMini] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLHotelMini].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLHotelMini] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLHotelMini] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLHotelMini] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLHotelMini] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLHotelMini] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLHotelMini] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QLHotelMini] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [QLHotelMini] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLHotelMini] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLHotelMini] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLHotelMini] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLHotelMini] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLHotelMini] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLHotelMini] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLHotelMini] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLHotelMini] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLHotelMini] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLHotelMini] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLHotelMini] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLHotelMini] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLHotelMini] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLHotelMini] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLHotelMini] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLHotelMini] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLHotelMini] SET  MULTI_USER 
GO
ALTER DATABASE [QLHotelMini] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLHotelMini] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLHotelMini] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLHotelMini] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [QLHotelMini]
GO
/****** Object:  Table [dbo].[Gia]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gia](
	[loaiphong] [nvarchar](10) NOT NULL,
	[giatheogio] [float] NULL,
	[giaquadem] [float] NULL,
	[giatheongay] [float] NULL,
 CONSTRAINT [PK_Giá] PRIMARY KEY CLUSTERED 
(
	[loaiphong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[tenkh] [nvarchar](30) NOT NULL,
	[cmnd] [nvarchar](11) NULL,
	[diachi] [nvarchar](50) NULL,
	[sodt] [nvarchar](11) NULL,
	[gioitinh] [bit] NULL,
	[giovao] [datetime] NULL,
	[loaithue] [nvarchar](10) NULL,
	[maphong] [int] NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[tenkh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiPhong]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiPhong](
	[loaiphong] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_LoaiPhong] PRIMARY KEY CLUSTERED 
(
	[loaiphong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LuongNV]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LuongNV](
	[mavaitro] [int] NOT NULL,
	[vaitro] [nvarchar](20) NULL,
	[luong] [int] NULL,
 CONSTRAINT [PK_LuongNV] PRIMARY KEY CLUSTERED 
(
	[mavaitro] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NguoiDung]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NguoiDung](
	[Taikhoan] [varchar](20) NOT NULL,
	[matkhau] [varchar](20) NULL,
	[mavaitro] [int] NULL,
 CONSTRAINT [PK_NguoiDung] PRIMARY KEY CLUSTERED 
(
	[Taikhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhanVien](
	[ten] [nvarchar](30) NOT NULL,
	[ngaysinh] [datetime] NULL,
	[diachi] [nvarchar](40) NULL,
	[sodt] [char](11) NULL,
	[gioitinh] [bit] NULL,
	[mavaitro] [int] NOT NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[ten] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[oder]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[oder](
	[stt] [int] NOT NULL,
	[loai] [nvarchar](10) NOT NULL,
	[mathang] [nvarchar](20) NULL,
	[dongia] [int] NULL,
	[donvi] [nvarchar](10) NULL,
 CONSTRAINT [PK_Order_1] PRIMARY KEY CLUSTERED 
(
	[stt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OderFood]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OderFood](
	[tenphong] [nvarchar](10) NULL,
	[mathang] [nvarchar](20) NULL,
	[soluong] [int] NULL,
	[dongia] [int] NULL,
	[tongtien] [int] NULL,
	[loai] [nvarchar](10) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Phong]    Script Date: 11/3/2017 6:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Phong](
	[maphong] [int] NOT NULL,
	[tenphong] [nvarchar](10) NULL,
	[loaiphong] [nvarchar](10) NULL,
 CONSTRAINT [PK_Phong] PRIMARY KEY CLUSTERED 
(
	[maphong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Gia] ([loaiphong], [giatheogio], [giaquadem], [giatheongay]) VALUES (N'Phòng lớn', 70000, 120000, 250000)
INSERT [dbo].[Gia] ([loaiphong], [giatheogio], [giaquadem], [giatheongay]) VALUES (N'Phòng nhỏ', 50000, 100000, 200000)
INSERT [dbo].[KhachHang] ([tenkh], [cmnd], [diachi], [sodt], [gioitinh], [giovao], [loaithue], [maphong]) VALUES (N'Duy Hân', N'321', N'Chợ Cây Điệp', N'1646887439', 1, CAST(N'2017-11-01 20:59:00.000' AS DateTime), N'Theo giờ', 1)
INSERT [dbo].[KhachHang] ([tenkh], [cmnd], [diachi], [sodt], [gioitinh], [giovao], [loaithue], [maphong]) VALUES (N'Nguyễn Hoàng Kiều Trinh', N'123', N'TPHCM', N'264897132', 0, CAST(N'2017-10-28 14:00:00.000' AS DateTime), N'Theo giờ', 5)
INSERT [dbo].[KhachHang] ([tenkh], [cmnd], [diachi], [sodt], [gioitinh], [giovao], [loaithue], [maphong]) VALUES (N'Quý', N'111', N'Phạm Văn Đồng', N'564987', 1, CAST(N'2017-11-03 17:52:00.000' AS DateTime), N'Theo giờ', 2)
INSERT [dbo].[KhachHang] ([tenkh], [cmnd], [diachi], [sodt], [gioitinh], [giovao], [loaithue], [maphong]) VALUES (N'Trịnh Văn Thành', N'456', N'Phạm Văn Đồng', N'987654321', 1, CAST(N'2017-11-02 10:49:00.000' AS DateTime), N'Theo giờ', 3)
INSERT [dbo].[LoaiPhong] ([loaiphong]) VALUES (N'Phòng lớn')
INSERT [dbo].[LoaiPhong] ([loaiphong]) VALUES (N'Phòng nhỏ')
INSERT [dbo].[LuongNV] ([mavaitro], [vaitro], [luong]) VALUES (1, N'Kế toán', 4500000)
INSERT [dbo].[LuongNV] ([mavaitro], [vaitro], [luong]) VALUES (2, N'Phục vụ phòng', 4000000)
INSERT [dbo].[LuongNV] ([mavaitro], [vaitro], [luong]) VALUES (3, N'Tiếp tân', 5000000)
INSERT [dbo].[NguoiDung] ([Taikhoan], [matkhau], [mavaitro]) VALUES (N'ketoan', N'321', 1)
INSERT [dbo].[NguoiDung] ([Taikhoan], [matkhau], [mavaitro]) VALUES (N'tieptan', N'123', 3)
INSERT [dbo].[NhanVien] ([ten], [ngaysinh], [diachi], [sodt], [gioitinh], [mavaitro]) VALUES (N'Hân', CAST(N'1998-02-11 17:34:00.000' AS DateTime), N'Ung Văn Khiêm', N'123456     ', 0, 3)
INSERT [dbo].[NhanVien] ([ten], [ngaysinh], [diachi], [sodt], [gioitinh], [mavaitro]) VALUES (N'Kiều Trinh', CAST(N'2017-11-01 10:44:00.000' AS DateTime), N'TP HCM', N'124545     ', 1, 1)
INSERT [dbo].[NhanVien] ([ten], [ngaysinh], [diachi], [sodt], [gioitinh], [mavaitro]) VALUES (N'Trinh', CAST(N'2017-11-01 10:44:00.000' AS DateTime), N'Chợ Đầu Mối', N'987654321  ', 0, 2)
INSERT [dbo].[oder] ([stt], [loai], [mathang], [dongia], [donvi]) VALUES (1, N'Thức Ăn', N'Snack', 12000, N'Gói')
INSERT [dbo].[oder] ([stt], [loai], [mathang], [dongia], [donvi]) VALUES (2, N'Thức Ăn', N'Bò khô', 15000, N'Gói')
INSERT [dbo].[oder] ([stt], [loai], [mathang], [dongia], [donvi]) VALUES (3, N'Thức Uống', N'Aquafina', 10000, N'Chai')
INSERT [dbo].[oder] ([stt], [loai], [mathang], [dongia], [donvi]) VALUES (4, N'Thức Uống', N'Sting', 13000, N'Chai')
INSERT [dbo].[oder] ([stt], [loai], [mathang], [dongia], [donvi]) VALUES (5, N'Thức Uống', N'Pepsi', 13000, N'Chai')
INSERT [dbo].[oder] ([stt], [loai], [mathang], [dongia], [donvi]) VALUES (6, N'Thức Ăn', N'Mì gói Omachi', 8000, N'Gói')
INSERT [dbo].[OderFood] ([tenphong], [mathang], [soluong], [dongia], [tongtien], [loai]) VALUES (N'Phòng 03', N'Bò khô', 1, 15000, 15000, N'Thức Ăn')
INSERT [dbo].[OderFood] ([tenphong], [mathang], [soluong], [dongia], [tongtien], [loai]) VALUES (N'Phòng 01', N'Pepsi', 3, 13000, 39000, N'Thức Uống')
INSERT [dbo].[OderFood] ([tenphong], [mathang], [soluong], [dongia], [tongtien], [loai]) VALUES (N'Phòng 01', N'Snack', 1, 12000, 12000, N'Thức Ăn')
INSERT [dbo].[OderFood] ([tenphong], [mathang], [soluong], [dongia], [tongtien], [loai]) VALUES (N'Phòng 03', N'Sting', 1, 13000, 13000, N'Thức Uống')
INSERT [dbo].[OderFood] ([tenphong], [mathang], [soluong], [dongia], [tongtien], [loai]) VALUES (N'Phòng 05', N'Pepsi', 1, 13000, 13000, N'Thức Uống')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (0, NULL, NULL)
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (1, N'Phòng 01', N'Phòng nhỏ')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (2, N'Phòng 02', N'Phòng lớn')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (3, N'Phòng 03', N'Phòng nhỏ')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (4, N'Phòng 04', N'Phòng lớn')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (5, N'Phòng 05', N'Phòng nhỏ')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (6, N'Phòng 06', N'Phòng lớn')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (7, N'Phòng 07', N'Phòng nhỏ')
INSERT [dbo].[Phong] ([maphong], [tenphong], [loaiphong]) VALUES (8, N'Phòng 08', N'Phòng lớn')
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_Phong] FOREIGN KEY([maphong])
REFERENCES [dbo].[Phong] ([maphong])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_Phong]
GO
ALTER TABLE [dbo].[NguoiDung]  WITH CHECK ADD  CONSTRAINT [FK_NguoiDung_LuongNV] FOREIGN KEY([mavaitro])
REFERENCES [dbo].[LuongNV] ([mavaitro])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NguoiDung] CHECK CONSTRAINT [FK_NguoiDung_LuongNV]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_LuongNV] FOREIGN KEY([mavaitro])
REFERENCES [dbo].[LuongNV] ([mavaitro])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_LuongNV]
GO
ALTER TABLE [dbo].[Phong]  WITH CHECK ADD  CONSTRAINT [FK_Phong_LoaiPhong] FOREIGN KEY([loaiphong])
REFERENCES [dbo].[LoaiPhong] ([loaiphong])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Phong] CHECK CONSTRAINT [FK_Phong_LoaiPhong]
GO
USE [master]
GO
ALTER DATABASE [QLHotelMini] SET  READ_WRITE 
GO
